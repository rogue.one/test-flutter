import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterapp/bloc/recipe_bloc.dart';
import 'package:flutterapp/repositories/recipe_repository.dart';
import 'package:flutterapp/views/home_page.dart';

void main()=> runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TestFlutter',
      theme: ThemeData(
        primarySwatch: Colors.amber
      ),
      home: BlocProvider(
        create: (BuildContext context) => RecipeBloc(repository: RecipeRepositoryImpl()),
        child: HomePage(),
      ),
    );
  }}

