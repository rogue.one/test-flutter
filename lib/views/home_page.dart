import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterapp/bloc/recipe_bloc.dart';
import 'package:flutterapp/bloc/recipe_event.dart';
import 'package:flutterapp/bloc/recipe_state.dart';
import 'package:flutterapp/models/recipe.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();

}
class _HomePageState extends State<HomePage> {
  RecipeBloc recipeBloc;
  @override
  void initState() {
    super.initState();
    recipeBloc = BlocProvider.of<RecipeBloc>(context);
    recipeBloc.add(FetchRecipeEvent());
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Builder(
        builder: (context) {
          return Material(
            child: Scaffold(
              appBar: AppBar(
                title: Text('List of recipes'),
                actions: <Widget>[
                  IconButton(
                    icon: Icon(Icons.autorenew),
                  ),
                ],
              ),
              body: Container(
                child: BlocListener<RecipeBloc, RecipeState> (
                  listener: (context, state){
                    if(state is RecipeFailedState) {
                      Scaffold.of(context).showSnackBar(
                        SnackBar(content: Text(state.message)),
                      );
                    }
                  },
                  child: BlocBuilder<RecipeBloc, RecipeState>(
                  builder: (context, state){
                    if (state is RecipeInitialState) {
                      return buildLoading();
                    } else if (state is RecipeLoadingState) {
                      return buildLoading();
                    } else if (state is RecipeLoadedState) {
                      return buildRecipeList(state.recipes);
                    } else if (state is RecipeFailedState) {
                      return buildErrorUi(state.message);
                    } else {
                      return BlocBuilder();
                    }
                  },
                )
                ),
              ),
            ),
          );
        },
      ),
    );
  }
  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }
  Widget buildErrorUi(String message) {
    return Center(
      child: Padding(
        padding: const  EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  Widget buildRecipeList(List<Recipes> recipes) {
    return ListView.builder(
      itemCount: recipes.length,
      itemBuilder: (ctx, pos) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            child: ListTile(
               leading: ClipOval(
                child: Hero(
                  tag: recipes[pos].images,
                  child: recipes[pos].images != null ? Image.network(
                    recipes[pos].images.first,
                    fit: BoxFit.cover,
                    height: 70,
                    width: 70,
                  ) : Container(),
                ),
               ),
              title: Text(recipes[pos].name),
              subtitle: Text(recipes[pos].description, overflow: TextOverflow.fade, maxLines: 2),
            ),
          ),
        );
      });
  }
}
