class Recipe {

  List<Recipes> recipes;

  Recipe({this.recipes});

  Recipe.fromJson(Map<String, dynamic> json) {
    if (json['recipes'] != null) {
      recipes = new List<Recipes>();
      json['recipes'].forEach((v) {
        recipes.add(new Recipes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.recipes != null) {
      data['recipes'] = this.recipes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Recipes {
  String uuid;
  String name;
  List<String> images;
  int lastUpdated;
  String description;
  String instructions;
  int difficulty;

  Recipes(
      {this.uuid,
        this.name,
        this.images,
        this.lastUpdated,
        this.description,
        this.instructions,
        this.difficulty});

  Recipes.fromJson(Map<String, dynamic> json) {
    uuid = json['uuid'];
    name = json['name'];
    images = json['images'].cast<String>();
    lastUpdated = json['lastUpdated'];
    description = json['description'];
    instructions = json['instructions'];
    difficulty = json['difficulty'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['uuid'] = this.uuid;
    data['name'] = this.name;
    data['images'] = this.images;
    data['lastUpdated'] = this.lastUpdated;
    data['description'] = this.description;
    data['instructions'] = this.instructions;
    data['difficulty'] = this.difficulty;
    return data;
  }
}