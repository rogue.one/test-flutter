import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutterapp/models/recipe.dart';
import 'dart:convert';

class General extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return GeneralState();
  }
}

class GeneralState extends State<General> {

  var isLoading = false;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('PracticeMakesPerfect'),
      ),
      body: isLoading ? Center(
        child: CircularProgressIndicator(),
      ) :
       Container(
        child: ListView(
          children: _recipeList(),
        ),
      ),
      floatingActionButton: Container(
        width: 70,
        height: 70,
        child: FloatingActionButton(
          onPressed: () => getRecipes(),
          child: Icon(Icons.find_replace),
        ),
      ),
    );
  }
  List<Recipe> recipes = List<Recipe>();
  final String apiUrl  = 'https://test.kode-t.ru/recipes';
  Future<List<Recipe>> getRecipes() async{
     setState(() {
      isLoading = true;
    });
    final response = await http.get(apiUrl);
    if(response.statusCode == 200) {
      var allData =  (json.decode(response.body) as Map)['recipes'];
      var dataList = List<Recipe>();
      allData.forEach((value) {
       // var record = Recipe(name: value['name'], description: value['description'] /*, images: value['images']*/);
      //  dataList.add(record);
      });
      setState(() {
        isLoading = false;
        recipes = dataList;
      });
    }
    return recipes;
  }
  List<Widget> _recipeList() {
    return recipes.map((Recipe f) => ListTile(
   //   title: Text(f.name),
   //   subtitle: Text(f.description, overflow: TextOverflow.fade, maxLines: 2),
     // leading: CircleAvatar(backgroundImage: AssetImage(f.images.toString())),
    )).toList();
  }
}