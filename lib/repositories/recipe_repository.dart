import 'package:flutterapp/models/recipe.dart';
import 'package:flutterapp/resources/api_strings.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
abstract class RecipeRepository{
  Future<List<Recipes>> getRecipes();
}
class RecipeRepositoryImpl implements RecipeRepository {
  @override
  Future<List<Recipes>> getRecipes() async {
  var response = await http.get(ApiStrings.apiUrl);
  if(response.statusCode == 200) {
    var data = json.decode(response.body);
    List<Recipes> recipes = Recipe.fromJson(data).recipes;
    return recipes;
  }
  else {
    throw Exception();
  }
  }
}