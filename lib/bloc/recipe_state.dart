import 'package:equatable/equatable.dart';
import 'package:flutterapp/models/recipe.dart';
import 'package:meta/meta.dart';
abstract class RecipeState extends Equatable {}

class RecipeInitialState extends RecipeState {
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class RecipeLoadingState extends RecipeState {
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

// ignore: must_be_immutable
class RecipeLoadedState extends RecipeState {

  List<Recipes> recipes;
  RecipeLoadedState({@required this.recipes});
  @override

  List<Object> get props => throw UnimplementedError();
}

// ignore: must_be_immutable
class RecipeFailedState extends RecipeState {
  String message;
  RecipeFailedState({@required this.message});
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}