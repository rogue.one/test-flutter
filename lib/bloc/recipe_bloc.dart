import 'package:bloc/bloc.dart';
import 'package:flutterapp/bloc/recipe_event.dart';
import 'package:flutterapp/bloc/recipe_state.dart';
import 'package:flutterapp/models/recipe.dart';
import 'package:flutterapp/repositories/recipe_repository.dart';
import 'package:meta/meta.dart';
class RecipeBloc extends Bloc<RecipeEvent, RecipeState> {

  RecipeRepository repository;
  RecipeBloc({@required this.repository});
  @override
  // TODO: implement initialState
  RecipeState get initialState => RecipeInitialState();

  @override
  Stream<RecipeState> mapEventToState(RecipeEvent event) async* {
    if(event is FetchRecipeEvent ) {
      yield RecipeLoadingState();
      try {
        List<Recipes> recipes =  await repository.getRecipes();
        yield RecipeLoadedState(recipes: recipes);
      } catch(e) {
        yield RecipeFailedState(message: e.toString());
      }
    }
  }

}